package me.daniel.roomChat.repositories;

import me.daniel.roomChat.models.User;

public interface UserRepository extends MyBaseRepository<User, Integer> {
	User findByName(String name);
	User findByAlias(String alias);
	User findById(Integer id);
}
