package me.daniel.roomChat.services;

import org.springframework.stereotype.Service;

@Service
public class StringEditor {

	public String capitalize(String text) {
		StringBuilder textBuilder = new StringBuilder(text.toLowerCase());
		textBuilder.setCharAt(0, text.substring(0, 1).toUpperCase().charAt(0));
		return textBuilder.toString();
	}
}
