package me.daniel.roomChat.controllers;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import me.daniel.roomChat.models.User;

@Controller
public class LogInController {
	
	@GetMapping("/")
	public String logInForm(User user) {
		return "logInForm";
	}
	
	@PostMapping("/")
	public String checkUserInfo(@Valid User user, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
		if (bindingResult.hasErrors()) {
			return "logInForm";
		}
		redirectAttributes.addAttribute("name", user.getName());
		return "redirect:/welcome";
	}
}
