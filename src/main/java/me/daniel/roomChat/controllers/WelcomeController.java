package me.daniel.roomChat.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import me.daniel.roomChat.services.StringEditor;

@Controller
public class WelcomeController {
	@Autowired
	private StringEditor stringEditor;
	
	@GetMapping("/welcome")
	public String welcome(@RequestParam(name="name", required=false, defaultValue="użytkowniku") String name, Model model) {
		model.addAttribute("name", stringEditor.capitalize(name));
		return "welcome";
	}
}
