package me.daniel.roomChat.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import me.daniel.roomChat.models.User;
import me.daniel.roomChat.repositories.UserRepository;

@Controller
@RequestMapping("/admin")
public class AdminPanelController {
	@Autowired
	private UserRepository userRepository;
	
	@GetMapping("/addUser")
	public String addUser(User user) {
		return "addUserForm";
	}
	
	@PostMapping("/addUser")
	public String addUser(@Valid User user, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return "addUserForm";
		}
		userRepository.save(user);
		return "redirect:/";
	}
	
	@GetMapping(path="/getUser", params={"name"})
	public @ResponseBody String getUserByName(@RequestParam String name) {
		User user = userRepository.findByName(name);
		return user != null ? user.toString() : "Nie znaleziono użytkowika o imieniu " + name;
	}
	
	@GetMapping(path="/getUser", params={"id"})
	public @ResponseBody String getUserById(@RequestParam Integer id) {
		User user = userRepository.findById(id);
		return user != null ? user.toString() : "Nie znaleziono użtykownika o id " + id;
	}
	
	@GetMapping(path="/getUser", params={"alias"})
	public @ResponseBody String getUserByAlias(@RequestParam String alias) {
		User user = userRepository.findByAlias(alias);
		return user != null ? user.toString() : "Nie znaleziono użytkownika o aliasie " + alias; 
	}
	
	@GetMapping("/allUsers")
	public @ResponseBody Iterable<User> getAllUsers() {
		return userRepository.findAll();
	}
}
